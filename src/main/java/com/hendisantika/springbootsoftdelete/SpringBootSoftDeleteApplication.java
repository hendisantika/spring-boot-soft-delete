package com.hendisantika.springbootsoftdelete;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSoftDeleteApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSoftDeleteApplication.class, args);
    }

}
