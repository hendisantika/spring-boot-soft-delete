package com.hendisantika.springbootsoftdelete.entity.pk;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-soft-delete
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 30/07/20
 * Time: 06.01
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleUserPK implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long roleId;

    private Long userId;

}