package com.hendisantika.springbootsoftdelete.entity.pk;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-soft-delete
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 30/07/20
 * Time: 06.00
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PermissionRolePK implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long permissionId;

    private Long roleId;

}