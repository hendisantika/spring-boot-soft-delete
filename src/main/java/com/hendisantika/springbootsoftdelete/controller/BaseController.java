package com.hendisantika.springbootsoftdelete.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-soft-delete
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/08/20
 * Time: 18.56
 */
public class BaseController {
    @ResponseStatus(HttpStatus.NOT_FOUND)
    protected class GenericNotFoundException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        public GenericNotFoundException(String message) {
            super(message);
        }
    }
}
