package com.hendisantika.springbootsoftdelete.repository;

import com.hendisantika.springbootsoftdelete.entity.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-soft-delete
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 30/07/20
 * Time: 06.23
 */
@Repository
@Transactional
public interface UserRepository extends SoftDeletesRepository<User, Long> {

}