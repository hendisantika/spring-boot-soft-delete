package com.hendisantika.springbootsoftdelete.repository;

import com.hendisantika.springbootsoftdelete.entity.Permission;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-soft-delete
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 30/07/20
 * Time: 06.06
 */
@Repository
@Transactional
public interface PermissionRepository extends SoftDeletesRepository<Permission, Long> {

}