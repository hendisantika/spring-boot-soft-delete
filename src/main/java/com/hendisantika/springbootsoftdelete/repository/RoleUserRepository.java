package com.hendisantika.springbootsoftdelete.repository;

import com.hendisantika.springbootsoftdelete.entity.RoleUser;
import com.hendisantika.springbootsoftdelete.entity.pk.RoleUserPK;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-soft-delete
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 30/07/20
 * Time: 06.24
 */
@Repository
@Transactional
public interface RoleUserRepository extends SoftDeletesRepository<RoleUser, RoleUserPK> {

}