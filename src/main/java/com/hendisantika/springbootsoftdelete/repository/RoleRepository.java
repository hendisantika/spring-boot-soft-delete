package com.hendisantika.springbootsoftdelete.repository;

import com.hendisantika.springbootsoftdelete.entity.Role;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-soft-delete
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 30/07/20
 * Time: 06.25
 */
@Repository
@Transactional
public interface RoleRepository extends SoftDeletesRepository<Role, Long> {

}