package com.hendisantika.springbootsoftdelete.service;

import com.hendisantika.springbootsoftdelete.entity.pk.PermissionRolePK;
import com.hendisantika.springbootsoftdelete.repository.PermissionRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-soft-delete
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/08/20
 * Time: 18.51
 */
@Service
public class PermissionRoleServiceImpl extends BaseServiceImpl implements PermissionRoleService {

    @Autowired
    private PermissionRoleRepository permissionRoleServiceRepository;

    @Override
    public void softDelete(PermissionRolePK id) {
        permissionRoleServiceRepository.softDelete(id);
    }
}
