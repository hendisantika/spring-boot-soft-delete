package com.hendisantika.springbootsoftdelete.service;

import com.hendisantika.springbootsoftdelete.entity.pk.RoleUserPK;
import com.hendisantika.springbootsoftdelete.repository.RoleUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-soft-delete
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/08/20
 * Time: 18.54
 */
@Service
public class RoleUserServiceImpl extends BaseServiceImpl implements RoleUserService {

    @Autowired
    private RoleUserRepository roleUserServiceRepository;

    @Override
    public void softDelete(RoleUserPK id) {
        roleUserServiceRepository.softDelete(id);
    }
}
