package com.hendisantika.springbootsoftdelete.service;

import com.hendisantika.springbootsoftdelete.entity.pk.PermissionRolePK;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-soft-delete
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/08/20
 * Time: 18.50
 */
@Service
public interface PermissionRoleService extends BaseService {
    void softDelete(PermissionRolePK id);
}