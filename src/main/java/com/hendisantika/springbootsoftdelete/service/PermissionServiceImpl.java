package com.hendisantika.springbootsoftdelete.service;

import com.hendisantika.springbootsoftdelete.repository.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-soft-delete
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/08/20
 * Time: 18.52
 */
@Service
public class PermissionServiceImpl extends BaseServiceImpl implements PermissionService {

    @Autowired
    private PermissionRepository permissionRepository;

    @Override
    public void softDelete(Long id) {
        permissionRepository.softDelete(id);
    }
}
