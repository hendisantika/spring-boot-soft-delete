package com.hendisantika.springbootsoftdelete.service;

import com.hendisantika.springbootsoftdelete.entity.pk.RoleUserPK;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-soft-delete
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/08/20
 * Time: 18.53
 */
@Service
public interface RoleUserService extends BaseService {
    void softDelete(RoleUserPK id);
}